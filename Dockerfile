FROM python:3.9-alpine3.13

# Install dependencies
RUN apk update
RUN apk add --no-cache \
  ca-certificates \
  ffmpeg \
  libffi \
  libsodium \
  opus-dev
RUN apk add --no-cache --virtual .build-deps \
  gcc \
  git \
  libffi-dev \
  make \
  musl-dev
RUN apk add --no-cache --virtual .voice-build-deps \
  build-base \
  libffi-dev \
  libsodium-dev

# Add source
WORKDIR /app
COPY . .

# Install pip dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

# Clean up dependencies
RUN apk del .build-deps
RUN apk del .voice-build-deps

# Create volume for mapping the config
VOLUME /app/config

# Just allow anything to avoid musicbot complaining
RUN chmod 777 '/app'

ENV APP_ENV=docker
ENTRYPOINT ["python3", "run.py"]
