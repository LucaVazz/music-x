# MusicBot

MusicBot is the original Discord music bot written for [Python](https://www.python.org "Python homepage") 3.5+, using the [discord.py](https://github.com/Rapptz/discord.py) library. It plays requested songs from YouTube and other services into a Discord server (or multiple servers). Besides, if the queue becomes empty MusicBot will play through a list of existing songs with configuration. The bot features a permission system allowing owners to restrict commands to certain people. As well as playing songs, MusicBot is capable of streaming live media into a voice channel (experimental).


## Setup - OpenShift CD

The definitions for the deployment to OpenShift are placed inside the `deployment-template.yaml` file.

This file only get applied manually by running `oc process -f deployment-template.yaml | oc apply -f -`.

Deployments get triggered by new code being integrated into the master branch.
